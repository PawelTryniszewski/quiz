package pl.sda.RozwiazanieProwadzacego;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<String, List<SingleQuizQuestion>> categories = readAllCategoriesQuestions();

        System.out.println("Wybierz kategorię: ");
        ArrayList<String> categoryNames = new ArrayList<>(categories.keySet());
        for (int i = 0; i < categoryNames.size(); i++) {
            String categoryName = categoryNames.get(i);
            System.out.println("> (" + (i+1) + ") " + categoryName);
        }
        Scanner scanner = new Scanner(System.in);

        int userCategoryChoice = scanner.nextInt();

        String selectedCategoryName = categoryNames.get(userCategoryChoice-1);
        List<SingleQuizQuestion> selectedCategoryQuestions = categories.get(selectedCategoryName);

        Collections.shuffle(selectedCategoryQuestions);

        int points = 0;
        for (int i = 0; i < 10; i++) {
            SingleQuizQuestion quizQuestion = selectedCategoryQuestions.get(i);
            System.out.println("Pytanie " + (i+1) + ": " + quizQuestion.question);
            List<String> answers = new ArrayList<>(quizQuestion.answers);
            String goodAnswer = answers.get(0);
            Collections.shuffle(answers);

            for (int j = 0; j < answers.size(); j++) {
                String answer = answers.get(j);
                System.out.println("  " + (j+1) + ") " + answer);
            }
            System.out.print("Wybrana odpowiedź: ");
            int selectedAnswer = scanner.nextInt();
            if (answers.get(selectedAnswer - 1).equals(goodAnswer)) {
                System.out.println("Dobra odpowiedź!");
                points++;
            } else {
                System.out.println("Błędna odpowiedź, prawidłowa to " + goodAnswer);
            }
        }
        System.out.println("Koniec, zdobyłeś " + points + "/10 punktów!");
    }

    private static Map<String, List<SingleQuizQuestion>> readAllCategoriesQuestions() {
        Map<String, List<SingleQuizQuestion>> categories = new HashMap<>();

        File folder = new File("D:\\Projekty\\quiz\\quiz");
        File[] allFiles = folder.listFiles();
        for (File file : allFiles) {
            List<SingleQuizQuestion> categoryQuestions = readAllQuizQuestionsFromFile(file);

            String categoryName = file.getName().replaceAll(".txt", "").replaceAll("_", " ");
            categories.put(categoryName, categoryQuestions);
        }

        return categories;
    }

    private static List<SingleQuizQuestion> readAllQuizQuestionsFromFile(File file) {
        List<SingleQuizQuestion> allQuestions = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                SingleQuizQuestion sqq = new SingleQuizQuestion();
                sqq.question = line;
                String answerNumberString = br.readLine();
                int answerNumber = Integer.parseInt(answerNumberString);
                for (int i = 0; i < answerNumber; i++) {
                    String answer = br.readLine();
                    sqq.answers.add(answer);
                }
                allQuestions.add(sqq);
            }
        } catch (IOException e) {
            System.err.println("Wczytanie pliku " + file.getName() + " nie powiodło się!");
            e.printStackTrace();
        }
        return allQuestions;
    }
}