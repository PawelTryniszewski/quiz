package pl.sda;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class quiz {
    public static volatile String a;

    public static void setA(String a) {
        quiz.a = a;
    }

    public static void main(String[] args) throws IOException, InterruptedException {

        final Map quiz = new HashMap();

        Scanner sc = new Scanner(System.in);
        String komenda;
        int counter = 0;
        int gamesCounter = 0;
        int totalWin = 0;
        System.out.println("Wpisz 'wczytaj', zeby zaladowac quiz");
        do {
            komenda = sc.nextLine();
            if (komenda.equalsIgnoreCase("wczytaj")) {
                quiz.putAll(wczytajPliki());
                System.out.println("Wpisz 'kategoria', zeby wybrac karegorie");
            }
            if (komenda.equalsIgnoreCase("kategoria")) {
                System.out.println(quiz.keySet());
                System.out.println("Wybierz kategorie");
                komenda = sc.nextLine();
                if (quiz.containsKey(komenda)) {
                    gamesCounter++;
                    //System.out.println(quiz.get(komenda));
                    List temp = new ArrayList();
                    temp.addAll((List) quiz.get(komenda));
                    List<Pytania> list = new ArrayList<Pytania>();
                    for (int i = 0; i < temp.size(); i++) {
                        if (temp.get(i).equals("2")) {
                            list.add(new Pytania(new ArrayList(Arrays.asList(temp.get(i - 1)
                                    , temp.get(i + 1), temp.get(i + 2)))));
                            i++;
                            i++;
                        }
                        if (temp.get(i).equals("4")) {
                            list.add(new Pytania(new ArrayList(Arrays.asList(temp.get(i - 1)
                                    , temp.get(i + 1), temp.get(i + 2), temp.get(i + 3), temp.get(i + 4)))));
                            i++;
                            i++;
                            i++;
                            i++;
                        }
                    }
                    Random random = new Random();
                    Set nowySet = new HashSet();
                    while (nowySet.size() < 10) {
                        nowySet.add(list.get(random.nextInt(list.size())));
                    }
                    List<Pytania> nowaLista = new ArrayList(nowySet);
                    int rr;

                    for (int i = 0; i < nowaLista.size(); i++) {
                        System.out.println(nowaLista.get(i).getPytanka().get(0));
                        Set losowePytania = new HashSet();
                        while (losowePytania.size() < nowaLista.get(i).getPytanka().size() - 1) {
                            rr = random.nextInt((nowaLista.get(i).getPytanka().size()));
                            if (rr != 0) {
                                losowePytania.add(nowaLista.get(i).getPytanka().get(rr));
                            }
                            rr = 0;
                        }
                        System.out.println(losowePytania.toString());
                        //System.out.println(nowaLista.get(i).getPytanka().get(1));
                        System.out.println("Podaj odp");

                        if (a.equalsIgnoreCase(String.valueOf(nowaLista.get(i).getPytanka().get(1)))) {
                            counter++;
                            totalWin++;
                        }
                    }
                    System.out.println("Koniec quizu. poprawnych odp " + counter);
                    counter = 0;
                } else {
                    System.out.println("Nie ma takiej kategorii");
                }
            }
            if (komenda.equalsIgnoreCase("quit")) {
                System.out.println("Rozegrales gier: " + gamesCounter + " lacznie odpowiedzialez na " + totalWin + " pytan"
                        + " z " + gamesCounter * 10);
            }
        } while (!komenda.equalsIgnoreCase("quit"));
    }

    public static Map wczytajPliki() throws IOException {
        Map quiz = new HashMap();
        Map pytania = new HashMap();
        File file = new File("D:\\Projekty\\quiz\\quiz");
        File[] a = file.listFiles();
        for (int i = 0; i < file.listFiles().length; i++) {
            List listaPytan = new ArrayList();
            assert a != null;
            BufferedReader reader = new BufferedReader(new FileReader("D:\\Projekty\\quiz\\quiz\\" + a[i].getName()));
            String linia;
            while ((linia = reader.readLine()) != null) {
                listaPytan.add(String.valueOf(linia));
            }
            quiz.put(a[i].getName(), listaPytan);
        }
        return quiz;
    }
}
