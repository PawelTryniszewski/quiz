package pl.sda;

import java.util.ArrayList;
import java.util.List;

public class Pytania {
//    private final static Pytania instance = new Pytania();
//    public static Pytania getInstance(){
//        return instance;
//    }
    List pytanka = new ArrayList();

    public List getPytanka() {
        return pytanka;
    }

    public void setPytanka(List pytanka) {
        this.pytanka = pytanka;
    }

    public Pytania(List pytanka) {
        this.pytanka = pytanka;
    }

    @Override
    public String toString() {
        return "Pytania{" +
                "pytanka=" + pytanka +
                '}';
    }
}
